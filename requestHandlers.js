var querystring = require("querystring");
var fs = require("fs");

exports.start = function start(response, postData) {
  console.log("Request handler 'start' was called.");

  var body = '<html>'+
    '<head>'+
    '<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />'+
    '</head>'+
    '<body>'+
    '<form action="/upload" method="post">'+
    '<textarea name="text" rows="20" cols="60"></textarea>'+
    '<input type="submit" value="Submit text" />'+
    '</form>'+
    '</body>'+
    '</html>';

    response.writeHead(200, {"Content-Type": "text/html"});
    response.write(body);
    response.end();
}

exports.upload = function upload(response, postData) {
  console.log("Request handler 'upload' was called.");
  response.writeHead(200, {"Content-Type": "text/html"});
  response.write("You've sent the text: " + querystring.parse(postData)["text"]);
  response.end();
}


exports.download = function download(response, postData) {
  console.log("Request handler 'upload' was called.");
  var position = 0;
  var proc = 1;
  var dataLenght = 100;
  var data = new Buffer(dataLenght);
  //var lenghtComplete = 1;
  var fileName = "Gantz.2011.mkv";
  //var fileName = "q.jpg";

		fs.readFile(fileName, 'binary', function(error, content) {
			if(error) {
				console.log("error " + error);
				response.end();
				return;
			}
			response.writeHead(200, {
				"Content-Type": "application/download",
				'Content-Disposition': 'attachment;filename=\"' + fileName + '\"',
				'content-length': content.length
				});
			response.write(content, 'binary');
			response.end();
		});
}
